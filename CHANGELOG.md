# [0.9.1](https://github.com/mindslab-ai/minds-mrc/compare/38f7ec2...iss4)(2017-11-10)
<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->


### Bug Fixes

- **run(answer)**: 모델에서 정답을 추출하지 못한 경우 정해진 코드를 리턴하도록 기능 추가
([#21](https://github.com/mindslab-ai/minds-mrc/issues/26))([1a3c6d3](https://github.com/mindslab-ai/minds-mrc/commit/1a3c6d3920961e173de2eeb7d893ef4c909aa03e))
- **run(answer)**: 원본 문서에 띄어쓰기가 제대로 안 된 경우 출력 정답의 인덱스가 밀리는 현상
([#43](https://github.com/mindslab-ai/minds-mrc/issues/43))([5635719](https://github.com/mindslab-ai/minds-mrc/commit/5635719cf9871aba02f018b355e30827354dd45f))


### Features

- **run(prediction)**: 기 학습된 모델을 불러 와 실행하는 기능 grpc 서버화
([#18](https://github.com/mindslab-ai/minds-mrc/issues/18))([b588f44](https://github.com/mindslab-ai/minds-mrc/commit/b588f44984e315fac180af99ddf3a0cf68a1e3ff))
- **run(prediction)**:  개별 질문/단락이 들어왔을 때 정답을 리턴하는 기능(상동)
- **run(answer)**: 최종 정답을 형태소가 아닌 원본 문서에서 찾도록 하는 기능
([#19](https://github.com/mindslab-ai/minds-mrc/issues/19))([b588f44](https://github.com/mindslab-ai/minds-mrc/commit/b588f44984e315fac180af99ddf3a0cf68a1e3ff))
- **pre-processing(ko)**: 한국어 데이터를 전처리하여 학습을 할 수 있는 형태로 변형하는 기능


### Enhancements

- **common**: 기존 강원대 코드에서 디렉토리 구조 전체적 변경
([#4](https://github.com/mindslab-ai/minds-mrc/issues/4))([5372cc2](https://github.com/mindslab-ai/minds-mrc/commit/5372cc29ecb028e565fd67a59cbe4014106d8b12))
- **run(prediction)**:  grpc 서버, 클라이언트 테스트 스크립트 추가: `tests/server`, `tests/client`
([#18](https://github.com/mindslab-ai/minds-mrc/issues/18))([b588f44](https://github.com/mindslab-ai/minds-mrc/commit/b588f44984e315fac180af99ddf3a0cf68a1e3ff))
- **application**: 엑소브레인 시멘틱 검색기를 붙여 자동으로 본문을 가져와 MRC까지 수행하는 테스트 스크립트 추가: `tests/exo_search`
([e4535ea](https://github.com/mindslab-ai/minds-mrc/commit/e4535ea640776bc53effbf88cd530a7f7234193d))


### Breaking Changes
None - Initial release 
