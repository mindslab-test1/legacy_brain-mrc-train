# brain-mrc

## 버전
- v0.9.1
- release date: 2017.11.10

## 가이드라인
- [설치, 구동 및 학습 가이드라인 wiki 링크](https://github.com/mindslab-ai/brain-mrc/wiki)
- 실행 전에 `export MRC_HOME=[MRC 코드를 내려받은 위치]` 해줄 것
  - 예) `export MRC_HOME=~/minds-mrc`
  - `echo $MRC_HOME` 명령어로 확인 가능

## 환경
- 학습과 서버 구동은 python3.5.x 환경 필요
- 클라이언트 쪽은 python 버전 무관(2, 3 모두 가능)
- 자세한 환경 관련 가이드라인은 위 wiki 링크 참고 바랍니다

## 기능
- 현재는 실행 부분만 grpc 패키징 되어 있음. 다음 버전에서 학습 부분도 포함 예정
- 실행 테스트 서버, 클라이언트는 `tests/server/brain_mrc_server.py`, `tests/client/mrc_client_test.py` 참고 
- 엑소브레인 시멘틱 검색까지 연동하여 테스트하고 싶은 경우 데몬 서버는 동일하게 띄우고 `tests/exo_search/mrc_client_search.py` 클라이언트로 테스트

## 추가적으로 필요한 리소스
- 아래는 깃헙 업로드 용량 초과로 올리지 않음
- 실행을 위해 아래 파일들이 필요하니 혹시 없는 경우 사내 82, 83 서버에서 다운로드하기 바람
  - `$MRC_HOME/resources/kr` 안에 `kor_vector.nnlm.h100.utf8.txt` 벡터 파일이 존재해야 함
  - `$MRC_HOME/models` 안에 모델 파일이 존재해야 함(`.pt` 확장자)
  - `$MRC_HOME/data/[project]` 안에 train, dev msgpack 파일이 존재해야 함(`.msgpack` 확장자)

## 디렉토리 구조
- `log`: `tests/server`에 들어 있는 데몬 서버를 실행할 경우 로그 파일이 생성되는 곳. 데몬을 실행할 때의 시간으로 파일명 자동 생성됨
- `proto`
- `pysrc`: 코어 부분의 모든 코드가 포함된 디렉토리
- `resources`: 필요한 리소스들이 포함된 디렉토리. 아래 '필요한 리소스' 항목을 참고하여 깃헙에 없는 것들은 다른 서버에서 복사해 와야 함
  - `kr`: 한국어 리소스
  - `en`: 영어 리소스(현재는 비어 있음)
- `server`: 데몬 서버를 실행시키는 스크립트가 포함된 디렉토리
- `tests`: 데몬 실행과 클라이언트 쪽 코드가 포함된 디렉토리
  - `client`
  - `exo_search`: 엑소브레인 시멘틱 검색까지 연동하여 테스트할 수 있는 코드
    - `minds`
    - `proto`
    - `testcase_sample`
    - `utils`
- `CHANGELOG.md`: 릴리즈 단위의 change log
- `requirements.txt`: 필요한 python 라이브러리 목록
- `README.md`

## 컴포넌트 구분
- common: 컴포넌트를 특정할 수 없는 경우
- pre-processing(common): 학습을 위한 정제 과정 중 언어 무관하게 공통인 부분
- pre-processing(ko): 학습을 위한 정제 과정 중 한국어에 초점이 맞춰진 부분
- pre-processing(en): 학습을 위한 정제 과정 중 영어에 초점이 맞춰진 부분
- run(prediction): 실행 부분 중 모델이 인덱스 형태로 예측값을 도출하는 부분까지를 지칭
- run(answer): 인덱스 형태의 예측값을 사람이 이해할 수 있는 최종 정답으로 변환하는 부분까지를 지칭
- train: 학습 관련된 모든 단계
- client: 클라이언트 측과 관련된 기능
- application: MRC를 이용하여 다른 모듈과 융합했거나 응용한 경우의 기능
