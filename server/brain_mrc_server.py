# coding=<utf-8>
# vi:ts=4:shiftwidth=4:expandtab

import os, sys
import re
import time
import random
import string
import logging
import argparse
import pickle
import collections
from concurrent import futures

import numpy
import torch
import msgpack
import grpc

sys.path.append(os.path.join(os.environ['MRC_HOME'], 'pysrc'))
from model import DocReaderModel
from prepro_utils_p3 import PreproUtils
from squad_prepro_p3 import m_lst2lst
from minds_mrc_pb2 import MRCServicer, MrcOutputs, add_MRCServicer_to_server


class MRC(MRCServicer):

    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Train a Document Reader model.'
        )
        ### system
        parser.add_argument('--project_dir', help='You MUST define the project directory name')  # not default
        parser.add_argument('--log_per_updates', type=int, default=1000,
                            help='log model loss per x updates (mini-batches).')
        parser.add_argument('--lang', default='kr', help='supported types: en, kr')
        parser.add_argument('--data_dir', default='data', help='default dir is $MRC_HOME/data')
        parser.add_argument('--train_data', default='tr.msgpack', help='path to train data file.')
        parser.add_argument('--dev_data', default='dev.msgpack', help='path to test data file.')
        parser.add_argument('--emb_file', default='resources/kr/kor_vector.nnlm.h100.utf8.txt', help='path to word embedding file.')
        parser.add_argument('--embedding_dim', type=int, default=100)
        parser.add_argument('--model_dir', default='models', help='path to store saved models.')
        parser.add_argument('--dont_save_last_only', action='store_false', dest='save_last_only', help='save models always.')
        parser.add_argument('--save_min_f1', type=float, default=75.0, help='save when F1 is greater than min_f1.')
        parser.add_argument('--eval_per_epoch', type=int, default=1, help='perform evaluation per x epoches.')
        parser.add_argument('--seed', type=int, default=411, help='random seed for data shuffling, dropout, etc.')
        parser.add_argument('--cuda', type=bool, default=torch.cuda.is_available(), help='whether to use GPU acceleration.')

        ### test
        parser.add_argument('-t', '--test', default='test_model_not_defined',
                            help='previous model file name (in `model_dir`). '
                                 'e.g. "best_model.pt"')
        parser.add_argument('-bs', '--batch_size', type=int, default=32)

        ### model
        parser.add_argument('--model_type', default='drqa+sm', help='supported types: drqa, drqa+sm, bidaf, r-net')
        parser.add_argument('--question_merge', default='self_attn', help='self_attn or avg')
        parser.add_argument('--doc_layers', type=int, default=5)
        parser.add_argument('--question_layers', type=int, default=5)
        parser.add_argument('--modeling_layers', type=int, default=2,
                            help='after BIDAF, Match LSTM, or RNN (for BIDAF, R-Net, DrQA+SM)')
        parser.add_argument('--modeling2_layers', type=int, default=2,
                            help='after Self-Matching Attention (for R-Net)')
        parser.add_argument('--hidden_size', type=int, default=100)
        parser.add_argument('--use_char', dest='use_char', action='store_true')
        parser.add_argument('--filter_sizes', default='3,4,5', help='for char CNN')
        parser.add_argument('--num_filters', type=int, default=30, help='for char CNN')
        parser.add_argument('--char_embed_dim', type=int, default=50)
        parser.add_argument('--char_embed_dim2', type=int, default=50)
        parser.add_argument('--exact_match', dest='exact_match', action='store_true')
        parser.add_argument('--exact_match2', dest='exact_match2', action='store_true')
        parser.add_argument('--num_features', type=int, default=2)
        parser.add_argument('--use_qemb', action='store_true')
        parser.add_argument('--use_qemb2', action='store_true')
        parser.add_argument('--concat_rnn_layers', action='store_true', help='for DrQA (BIDAF and R-Net ignore this option)')
        parser.add_argument('--sum_rnn_layers', dest='sum_rnn_layers', action='store_true')
        parser.add_argument('--dropout_emb', type=float, default=0.2)
        parser.add_argument('--dropout_rnn', type=float, default=0.2)
        parser.add_argument('--no_dropout_rnn_output', dest='dropout_rnn_output', action='store_false')
        parser.add_argument('--max_len', type=int, default=50)
        parser.add_argument('--rnn_type', default='sru', help='supported types: rnn, gru, lstm, sru')

        self.args = parser.parse_args()
        self.s_util = PreproUtils()

        # set model dir
        self.model_dir = os.path.join(os.environ['MRC_HOME'], self.args.model_dir)
        os.makedirs(self.model_dir, exist_ok=True)
        self.model_dir = os.path.abspath(self.model_dir)

        # set random seed
        random.seed(self.args.seed)
        torch.manual_seed(self.args.seed)
        if self.args.cuda:
            torch.cuda.manual_seed(self.args.seed)
        # setup logger
        self.log = self.set_logger()
        # load model
        self.model, self.opt, self.vocab_x_list = self.load_model()

    def set_logger(self):
        log = logging.getLogger(__name__)
        log.setLevel(logging.INFO)
        # create log file: title is start time
        os.environ['TZ'] = 'Asia/Seoul'
        time.tzset()
        if not os.path.exists(os.path.join(os.environ['MRC_HOME'], 'log')):
            os.makedirs(os.path.join(os.environ['MRC_HOME'], 'log'))
        fh = logging.FileHandler(os.path.join(os.environ['MRC_HOME'], 'log',
                                              '{}.log'.format(time.strftime("%Y%m%d_%H%M%S"))))
        fh.setLevel(logging.DEBUG)
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.INFO)
        formatter = logging.Formatter(fmt='[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s',
                                      datefmt='%Y-%m-%d_%T %Z')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        log.addHandler(fh)
        #log.addHandler(ch)
        return log

    def create_voca_dic(self, voca_list):
        vocab_dic = {'<PAD>':0, '<UNK>':1}
        for i, line in enumerate(voca_list):
            vocab_dic[line] = i
        return vocab_dic, voca_list

    def load_model(self):
        self.log.info('[Server starts loading...]')
        dev, dev_ans_dict, vocab_x_list, embedding, opt =\
            load_data(vars(self.args))  # TODO dev, dev_ans_dict: not used
        self.log.info('[Data loaded.]')
        self.vocab_dic, self.voca_list = self.create_voca_dic(vocab_x_list)

        self.log.info('[loading previous model...]')
        checkpoint = torch.load(os.path.join(os.environ['MRC_HOME'], self.args.test))
        opt = checkpoint['config']
        opt['max_len'] = self.args.max_len
        self.log.info('max_len: {}'.format(opt['max_len']))
        state_dict = checkpoint['state_dict']
        model = DocReaderModel(opt, embedding, state_dict)
        if self.args.cuda:
            model.cuda()
        self.log.info('[Server has been loaded...]')
        return model, opt, vocab_x_list

    # main
    def predict_main(self, morph_q, morph_passage_list):
        x_lst = []
        q_lst = []
        a_lst = [[None]] # fake data
        x_str = []
        q_str = []
        qids = [[None]] # fake data
        que = morph_q

        mo_q = m_lst2lst(que)
        for morph_passage in morph_passage_list:
            self.log.debug("morph_passage: {}".format(morph_passage))
            mo_cont = m_lst2lst(morph_passage)  # flatten
            mo_cont.append('</s>')
            ph_dic = {'ph_nnp':{}, 'ph':{}}
            q_lst.append(self.s_util.make_idx_list(mo_q, self.vocab_dic, ph_dic))
            x_lst.append(self.s_util.make_idx_list(mo_cont, self.vocab_dic, ph_dic))
            q_str.append(mo_q)
            x_str.append(mo_cont)
        _, _, dev_xc, dev_qc, _ = self.s_util.make_char_vocab(x_str, q_str, x_str, q_str, self.opt['char2id'],
                                                              os.path.join(os.environ['MRC_HOME'],
                                                                           self.args.data_dir,
                                                                           self.args.project_dir))

        # dev_x, dev_q, dev_xc, dev_qc, dev_y, dev_x_str, dev_q_str, dev_qids
        dev = self.create_batch(x_lst, q_lst, dev_xc, dev_qc, a_lst*len(x_lst),
                                x_str, q_str, qids*len(x_lst))
        batches = BatchGen(dev, batch_size=self.args.batch_size, evaluation=True, gpu=self.args.cuda,
                           use_char=self.opt['use_char'], padding_idx=self.opt['padding_idx'],
                           char_padding_idx=self.opt['char_padding_idx'])
        predictions = []
        score_lst = []
        idx_tuple_list = []
        start_time = time.time()
        cnt = 1
        for batch in batches:
            try:
                pred, pred_score, idx_tuple = self.model.predict(batch)
                self.log.info("prediction running time {}: {}".format(cnt, time.time()-start_time))
            except RuntimeError:
                self.log.info("prediction running time {}: {}".format(cnt, time.time()-start_time))
                self.log.error("Output size is too small. (There's no answer)")
                # empty answer
                predictions.extend(["|$|$|NO_ANSWER1|$|$|"])
                score_lst.extend([0])
                idx_tuple_list.append(tuple())
                cnt += 1
                continue
            # normalize prediction and answer
            new_pred = []
            for p in pred:
                p = p.replace(' \'s', '\'s')
                p = p.replace(' ,', ',')
                p = p.replace(' - ', '-')
                p = p.replace(' / ', '/')
                p = p.replace(' ( ', ' (')
                p = p.replace(' ) ', ') ')
                p = p.replace("do n't", "don't")
                p = p.replace('US$ ', 'US$')
                p = p.replace('5 k resolution', '5k resolution')
                new_pred.append(p)
            pred = new_pred
            # end
            predictions.extend(pred)
            score_lst.extend(pred_score)	# pred_score
            idx_tuple_list.append(idx_tuple)
            cnt += 1
        return predictions, score_lst, idx_tuple_list

    def map_original_idx(self, morph, original):
        flatten_morph = list()
        for s in morph:
            flatten_morph += s
        map_dic = dict()
        m_idx = 0
        o_idx = 0
        for m, o in zip(flatten_morph, original):
            max_o_idx = o_idx + len(o)
            if len(m) == 1:
                map_dic[m_idx] = (o_idx, o_idx+len(o))
                m_idx += 1
                o_idx += len(o) + 1
            else:
                pass_list = list()      # splitted morph ex. 나서+었
                start_from = 0
                fail = False  # beforehand
                for each_m in m:
                    splitted_m = each_m.rsplit("/", 1)[0]
                    location = o.find(splitted_m, start_from)
                    if location >= 0:
                        if fail:
                            o_idx += location
                            fail = False
                        map_dic[m_idx] = (o_idx, o_idx+len(splitted_m))
                        o_idx += len(splitted_m)
                        start_from = location
                    else:
                        pass_list.append((m_idx, m, start_from+o_idx, o))
                        fail = True
                    m_idx += 1
                pass_list_tmp = pass_list
                if len(pass_list) > 0:  # if there is a splitted morphs
                    for pass_item in pass_list:
                        # m_idx, m, start_position
                        backward_until = 1
                        while m_idx > (pass_item[0]+backward_until):
                            try:
                                #만약 그 다음 것도 찾지 못하면 하나 더 뒤로 감
                                map_dic[pass_item[0]] = (pass_item[2], map_dic[pass_item[0]+backward_until][0])
                                pass_list_tmp.remove(pass_item)
                                break
                            except KeyError:
                                backward_until += 1
                if len(pass_list_tmp) > 0:
                    for pass_item in pass_list_tmp:
                        map_dic[pass_item[0]] = (pass_item[2], max_o_idx)
                o_idx = max_o_idx
                o_idx += 1  # space
        # for </s> tag
        map_dic[m_idx] = (o_idx-1, o_idx-1)
        return map_dic

    def SendQuestion(self, request, context):
        mrc_outputs = MrcOutputs()
        # morph analyzer
        self.log.info("Received question: {}".format(eval(request.question)))
        morph_passage_list = list()
        original_passage_list = list()
        map_dic_list = list()
        for passage in request.passages:
            new_passage_original = " ".join(passage.words)
            self.log.debug("Original passage: {}".format(new_passage_original))
            self.log.debug("morph passage: {}".format(eval(passage.morp)))
            self.log.debug("word_list: {}".format(passage.words))
            # append original passages
            original_passage_list.append(new_passage_original)
            # append morph passages
            morph_passage_list.append(eval(passage.morp))
            # append idx map dict
            map_dic = self.map_original_idx(eval(passage.morp), passage.words)
            map_dic_list.append(map_dic)
        self.log.info("morph_passage_list: {}".format(morph_passage_list))

        prediction_list, loss_list, idx_t_list = self.predict_main(eval(request.question), morph_passage_list)
        for p, l, idx, ori_ctx, map_dic in zip(prediction_list, loss_list, idx_t_list, original_passage_list, map_dic_list):
            if p.startswith("|$|$|NO_ANSWER"):
                # can't find the answer in model
                self.log.info("There's no answer(model)")
                mrc_outputs.answers.add(answer=p, prob=l)  # answer="|$|$|NO_ANSWER1|$|$|", prob=0
            else:
                self.log.info("p: {}".format(p))
                self.log.debug("l: {}".format(l))
                self.log.info("idx: {}".format(idx))
                self.log.info("ori_ctx: {}".format(ori_ctx))
                self.log.debug("map_dic: {}".format(map_dic))
                s_idx = idx[0]
                e_idx = idx[1]
                real_answer = ori_ctx[map_dic[s_idx][0]:map_dic[e_idx][1]]
                self.log.info("prediction: {} ({})".format(p, l))
                self.log.info("real answer: {}".format(real_answer))
                if real_answer.strip() != "":
                    #mrc_outputs.answers.add(answer=p, prob=l)   # 형태소 단위 정답
                    mrc_outputs.answers.add(answer=real_answer, prob=l)  # 원본에서의 정답
                else:
                    self.log.info("There's no answer(post-processing)")
                    mrc_outputs.answers.add(answer="|$|$|NO_ANSWER2|$|$|", prob=l)
        return mrc_outputs

    # exact_match , TF feature
    def make_feature(self, train_x, train_q, train_x_str, train_q_str):
        for x, q, xs, qs in zip(train_x, train_q, train_x_str, train_q_str): # using word idx
            if len(x) != len(xs) or len(q) != len(qs):
                self.log.info(len(x), len(xs), len(q), len(qs))
                import pdb; pdb.set_trace()
        train_f = []
        #for x, q in zip(train_x, train_q): # using word idx
        for x, q in zip(train_x_str, train_q_str): # using word string
            f = [wid in q for wid in x]
            counter_ = collections.Counter(wid for wid in x)
            total = sum(counter_.values()) + 1e-5
            tf = [counter_[wid] / total for wid in x]
            train_f.append(list(zip(f, tf)))
        return train_f

    def create_batch(self, dev_x, dev_q, dev_xc, dev_qc, dev_y, dev_x_str, dev_q_str, dev_qids):
        dev_ys = [0]*len(dev_x)  # fake data
        dev_ye =[0]*len(dev_x)  # fake data
        dev_xf = self.make_feature(dev_x, dev_q, dev_x_str, dev_q_str)
        dev_qf = self.make_feature(dev_q, dev_x, dev_q_str, dev_x_str)
        self.log.info('exact_match and TF features are generated.')
        # train, dev data
        if self.opt['use_char']:
            dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_xc, dev_qc, dev_ys, dev_ye, dev_x_str, dev_qids))
        else:
            dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_ys, dev_ye, dev_x_str, dev_qids))
        return dev

def load_embedding(file_name, word2id_dic, vocab_size, wv_dim):
    """ Loads word vectors from word2vec embedding (key value1 value2 ...)
    """
    embedding = 0.01 * numpy.random.randn(vocab_size, wv_dim)
    #with open(file_name) as f:
    with open(file_name, encoding='utf8') as f:
        count = 0
        for line in f:
            elems = line.split()
            token = elems[0]
            if token in word2id_dic:
                embedding[word2id_dic[token]] = [float(v) for v in elems[-wv_dim:]]
                count += 1
            elif token.encode('utf8') in word2id_dic:
                embedding[word2id_dic[token.encode('utf8')]] = [float(v) for v in elems[-wv_dim:]]
                count += 1
            #else: print(token, end=' ')
        logging.info('load word embedding:', file_name, wv_dim, count)
    return embedding

def load_data(opt):
    # train set
    if 'pkl' in opt['train_data']:
        with open(os.path.join(os.environ['MRC_HOME'], opt['data_dir'], opt['project_dir'],
                               opt['train_data']), 'rb') as f:
            pickle_list = pickle.load(f, encoding='utf8')
    elif 'msgpack' in opt['train_data']:
        with open(os.path.join(os.environ['MRC_HOME'], opt['data_dir'], opt['project_dir'],
                               opt['train_data']), 'rb') as f:
            data = msgpack.load(f, encoding='utf8')
            pickle_list = data['x_lst'], data['q_lst'], data['x_c_lst'], data['q_c_lst'], data['a_lst'], data['x_str'], \
                          data['q_str'], data['qids'], data['voca_list'], data['char2id']
    else:
        print('Unknown file: {}', opt['train_data'])
        sys.exit()

    if len(pickle_list) == 6: # for squad5 data ?
        train_x, train_q, train_y, train_x_str, train_qids, vocab_x_list = pickle_list
        train_q_str = []
    elif len(pickle_list) == 7: # for Korean data
        train_x, train_q, train_y, train_x_str, train_q_str, train_qids, vocab_x_list = pickle_list
        #print('vocab_x:', vocab_x_list[:50])
    elif len(pickle_list) == 10: # for Korean data v7
        train_x, train_q, train_xc, train_qc, train_y, train_x_str, train_q_str, train_qids, vocab_x_list, opt['char2id'] = pickle_list
    elif len(pickle_list) == 11: # for squad6
        train_x, train_q, train_y, train_x_str, train_q_str, train_qids, _, _, _, _, vocab_x_list = pickle_list
    else:
        print('Unknown Pickle file:', len(pickle_list))
        sys.exit()
    # dev set
    if 'pkl' in opt['dev_data']:
        with open(os.path.join(os.environ['MRC_HOME'], opt['data_dir'], opt['project_dir'],
                               opt['dev_data']), 'rb') as f:
            pickle_list = pickle.load(f, encoding='utf8')
    elif 'msgpack' in opt['dev_data']:
        with open(os.path.join(os.environ['MRC_HOME'], opt['data_dir'], opt['project_dir'],
                               opt['dev_data']), 'rb') as f:
            data = msgpack.load(f, encoding='utf8')
            pickle_list = data['x_lst'], data['q_lst'], data['x_c_lst'], data['q_c_lst'], data['a_lst'], \
                          data['x_str'], data['q_str'], data['qids'], data['voca_list'], data['char2id']
    if len(pickle_list) == 6:
        dev_x, dev_q, dev_y, dev_x_str, dev_qids, _ = pickle_list
        dev_q_str = []
    elif len(pickle_list) == 7:
        dev_x, dev_q, dev_y, dev_x_str, dev_q_str, dev_qids, _ = pickle_list
    elif len(pickle_list) == 10:
        dev_x, dev_q, dev_xc, dev_qc, dev_y, dev_x_str, dev_q_str, dev_qids, _, _ = pickle_list
    elif len(pickle_list) == 11:
        dev_x, dev_q, dev_y, dev_x_str, dev_q_str, dev_qids, _, _, _, _, _ = pickle_list
    else:
        logging.info('Unknown Pickle file:', len(pickle_list))
        sys.exit()
    logging.info('train data size:', len(train_x), len(train_q), len(train_y), len(train_x_str), len(train_q_str), len(train_qids))
    logging.info('dev data size:', len(dev_x), len(dev_q), len(dev_y), len(dev_x_str), len(dev_q_str), len(dev_qids))
    if opt['use_char']:
        logging.info ('train char:', len(train_xc), len(train_qc)); print ('dev char:', len(dev_xc), len(dev_qc))
        opt['char_vocab_size'] = len(opt['char2id'])
        logging.info('\nchar_vocab_size:', opt['char_vocab_size'])
    # train_y -> train_ys and train ye
    train_ys = []
    train_ye = []
    for y in train_y:
        train_ys.append(int(y[1]))
        train_ye.append(int(y[2]))
    # dev_y -> dev_ys and dev ye
    dev_ys = []
    dev_ye = []
    for y in dev_y:
        dev_ys.append(int(y[1]))
        dev_ye.append(int(y[2]))
    logging.debug('dev_q[0]:', dev_q[0])
    if opt['use_char']: print('dev_qc[0]:', dev_qc[0])
    logging.debug('dev_y[0]:', dev_y[0])
    logging.debug('dev_ys[0]:', dev_ys[0])
    logging.debug('dev_ye[0]:', dev_ye[0])
    logging.debug('dev_qids[0]:', dev_qids[0])
    # load word vocab.
    opt['word2id'] = {}
    for i, w in enumerate(vocab_x_list):
        if w in opt['word2id']:
            try: logging.info('Warning(vocab_word):', w , 'is exist!', opt['word2id'][w], i)
            except: pass
        opt['word2id'][w] = i
    if 'eos' not in opt['word2id']:
        i += 1
        vocab_x_list.append('eos')
        opt['word2id']['eos'] = i
        logging.info('"eos" is appended to vocab.')
    logging.info('vocab_x size:', i+1)
    opt['pretrained_words'] = True
    opt['vocab_size'] = i+1
    # init char vocab.
    opt['char_padding_idx'] = 0
    # load word embedding
    emb_numpy = load_embedding(os.path.join(os.environ['MRC_HOME'], opt['emb_file']),
                               opt['word2id'], opt['vocab_size'], opt['embedding_dim'])
    embedding = torch.Tensor(emb_numpy)
    # padding_idx
    # eos for batch padding
    if '<PAD>' in opt['word2id']: padding_idx = opt['word2id']['<PAD>']
    elif '<pad>' in opt['word2id']: padding_idx = opt['word2id']['<pad>']
    elif '</s>' in opt['word2id']: padding_idx = opt['word2id']['</s>']
    elif 'EOS' in opt['word2id']: padding_idx = opt['word2id']['EOS']
    elif 'eos' in opt['word2id']: padding_idx = opt['word2id']['eos']
    elif 'UNK' in opt['word2id']: padding_idx = opt['word2id']['UNK']
    elif 'unk' in opt['word2id']: padding_idx = opt['word2id']['unk']
    else: padding_idx = 0
    opt['padding_idx'] = padding_idx
    print('batch padding idx:', padding_idx, vocab_x_list[padding_idx])
    # exact_match , TF feature
    def make_feature(train_x, train_q, train_x_str, train_q_str):
        for x, q, xs, qs in zip(train_x, train_q, train_x_str, train_q_str): # using word idx
            if len(x) != len(xs) or len(q) != len(qs):
                print(len(x), len(xs), len(q), len(qs))
                import pdb; pdb.set_trace()
        train_f = []
        #for x, q in zip(train_x, train_q): # using word idx
        for x, q in zip(train_x_str, train_q_str): # using word string
            f = [wid in q for wid in x]
            counter_ = collections.Counter(wid for wid in x)
            total = sum(counter_.values()) + 1e-5
            tf = [counter_[wid] / total for wid in x]
            train_f.append(list(zip(f, tf)))
        return train_f
    train_xf = make_feature(train_x, train_q, train_x_str, train_q_str)
    dev_xf = make_feature(dev_x, dev_q, dev_x_str, dev_q_str)
    train_qf = make_feature(train_q, train_x, train_q_str, train_x_str)
    dev_qf = make_feature(dev_q, dev_x, dev_q_str, dev_x_str)
    #print('dev_f[0]:', dev_f[0])
    print('exact_match and TF features are generated.')
    # train, dev data
    if opt['use_char']:
        train = list(zip(train_x, train_xf, train_q, train_qf, train_xc, train_qc, train_ys, train_ye, train_x_str, train_qids))
        dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_xc, dev_qc, dev_ys, dev_ye, dev_x_str, dev_qids))
    else:
        train = list(zip(train_x, train_xf, train_q, train_qf, train_ys, train_ye, train_x_str, train_qids))
        dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_ys, dev_ye, dev_x_str, dev_qids))
    # for dev_y_list (for multiple answers)
    dev_ans_dict = {}
    return dev, dev_ans_dict, vocab_x_list, embedding, opt

class BatchGen:
    def __init__(self, data, batch_size, gpu, evaluation=False, use_char=False, padding_idx=0, char_padding_idx=0):
        '''
        input:
            data - list of lists
            batch_size - int
        '''
        self.batch_size = batch_size
        self.evaluation = evaluation
        self.gpu = gpu
        self.use_char = use_char
        self.padding_idx = padding_idx
        self.char_padding_idx = char_padding_idx

        # shuffle
        if not evaluation:
            indices = list(range(len(data)))
            random.shuffle(indices)
            data = [data[i] for i in indices]
        # chunk into batches
        data = [data[i:i + batch_size] for i in range(0, len(data), batch_size)]
        self.data = data

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        for batch in self.data:
            batch_size = len(batch)
            batch = list(zip(*batch))
            if self.use_char:
                assert len(batch) == 10
            else:
                assert len(batch) == 8

            # batch: list(zip(train_x, train_xf, train_q, train_qf, train_ys, train_ye, train_x_str, train_qids))
            # use_char: list(zip(train_x, train_xf, train_q, train_qf, train_xc, train_qc, train_ys, train_ye, train_x_str, train_qids))
            # train_x
            context_x_len = max(len(x) for x in batch[0])
            context_id = torch.LongTensor(batch_size, context_x_len).fill_(self.padding_idx)
            for i, doc in enumerate(batch[0]):
                context_id[i, :len(doc)] = torch.LongTensor(doc)

            # train_xf
            context_xf_len = max(len(x) for x in batch[1])
            feature_len = len(batch[1][0][0])
            context_feature = torch.Tensor(batch_size, context_xf_len, feature_len).fill_(0)
            # test - by leeck
            if context_x_len != context_xf_len:
                print('Error: context_x_len != context_xf_len')
                print('context_x_len=', context_x_len, 'context_xf_len=', context_xf_len)
                print(context_id.size(), context_feature.size())
                import pdb; pdb.set_trace()
            for i, doc in enumerate(batch[1]):
                for j, feature in enumerate(doc):
                    context_feature[i, j, :] = torch.Tensor(feature)

            # train_q
            question_len = max(len(x) for x in batch[2])
            question_id = torch.LongTensor(batch_size, question_len).fill_(self.padding_idx)
            for i, doc in enumerate(batch[2]):
                try: question_id[i, :len(doc)] = torch.LongTensor(doc)
                except: import pdb; pdb.set_trace()

            # train_qf
            context_qf_len = max(len(x) for x in batch[3])
            feature_len = len(batch[3][0][0])
            question_feature = torch.Tensor(batch_size, context_qf_len, feature_len).fill_(0)
            # test - by leeck
            if question_len != context_qf_len:
                print('Error: question_len != context_qf_len')
                print('question_len=', question_len, 'context_f_len=', context_qf_len)
                print(question_id.size(), question_feature.size())
                import pdb; pdb.set_trace()
            for i, q in enumerate(batch[3]):
                for j, feature in enumerate(q):
                    question_feature[i, j, :] = torch.Tensor(feature)

            # mask
            context_mask = torch.eq(context_id, self.padding_idx)
            question_mask = torch.eq(question_id, self.padding_idx)

            if self.use_char:
                # train_xc
                context_x_len = max(len(x) for x in batch[4])
                context_c_len = -1
                for x in batch[4]:
                    max_c_len = max(len(c) for c in x)
                    if max_c_len > context_c_len: context_c_len = max_c_len
                context_cid = torch.LongTensor(batch_size, context_x_len, context_c_len).fill_(self.char_padding_idx)
                for i, x in enumerate(batch[4]):
                    for j, w in enumerate(x):
                        context_cid[i, j, :len(w)] = torch.LongTensor(w)
                # train_qc
                question_len = max(len(q) for q in batch[5])
                question_c_len = -1
                for q in batch[5]:
                    max_c_len = max(len(c) for c in q)
                    if max_c_len > question_c_len: question_c_len = max_c_len
                question_cid = torch.LongTensor(batch_size, question_len, question_c_len).fill_(self.char_padding_idx)
                for i, q in enumerate(batch[5]):
                    for j, w in enumerate(q):
                        try: question_cid[i, j, :len(w)] = torch.LongTensor(w)
                        except: import pdb; pdb.set_trace()
                # mask
                context_cmask = torch.eq(context_cid, self.char_padding_idx)
                question_cmask = torch.eq(question_cid, self.char_padding_idx)

            # train_ys, train_ye
            y_s = torch.LongTensor(batch[-4])
            y_e = torch.LongTensor(batch[-3])

            # train_x_str
            text = list(batch[-2])

            # train_qids
            qid = list(batch[-1])

            if self.gpu:
                context_id = context_id.pin_memory()
                context_feature = context_feature.pin_memory()
                context_mask = context_mask.pin_memory()
                question_id = question_id.pin_memory()
                question_mask = question_mask.pin_memory()
                y_s = y_s.pin_memory()
                y_e = y_e.pin_memory()
                if self.use_char:
                    context_cid = context_cid.pin_memory()
                    question_cid = question_cid.pin_memory()
                    context_cmask = context_cmask.pin_memory()
                    question_cmask = question_cmask.pin_memory()
            if self.use_char:
                yield (context_id, context_feature, context_mask, question_id, question_feature, question_mask, context_cid, context_cmask, question_cid, question_cmask, y_s, y_e, text, qid)
            else:
                yield (context_id, context_feature, context_mask, question_id, question_feature, question_mask, y_s, y_e, text, qid)

def _normalize_answer(s):
    def remove_articles(text):
        return re.sub(r'\b(a|an|the)\b', ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))

def _exact_match(pred, answers):
    if pred is None or answers is None:
        return False
    pred = _normalize_answer(pred)
    for a in answers:
        if pred == _normalize_answer(a):
            return True
    return False

if __name__ == '__main__':
    logging.info("Starting server...")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_MRCServicer_to_server(MRC(), server)
    server.add_insecure_port('[::]:50001')
    server.start()

    while True:
        # Sleep forever, since `start` doesn't block
        time.sleep(1)
