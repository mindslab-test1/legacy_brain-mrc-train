#! /usr/bin/python
# -*- coding: UTF-8 -*-

import os, sys
import time
import logging
from collections import Counter

import grpc

sys.path.append(os.path.join(os.environ['HOME'], 'minds_mrc', 'pysrc'))
from morp_analyze_my import NLPAnalyzer
from minds_mrc_pb2 import MrcInput, MrcPassage, MRCStub


def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

class MrcClient:
    search_channel = None
    search_client = None

    def __init__(self, mrc_address, mrc_port):
        self.DOC_TYPE = "news"
        self.ANSWER_THRESHOLD = 0.7
        self.mrc_channel = grpc.insecure_channel('{}:{}'.format(mrc_address, mrc_port))
        self.mrc_client = MRCStub(self.mrc_channel)
        # for time check
        self.entire_start_time = None
        self.start_time = None
        # set logger
        self.logger = self.set_logger()
        self.nlp = NLPAnalyzer()

    def set_logger(self):
        log = logging.getLogger(__name__)
        log.setLevel(logging.DEBUG)
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter(fmt='[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s',
                                      datefmt='%Y-%m-%d_%T %Z')
        ch.setFormatter(formatter)
        log.addHandler(ch)
        return log

    def time_check(self, stage):
        self.logger.info("{}: {:.5}s".format(stage, float(time.time()-self.start_time)))
        self.start_time = time.time()

    def mrc(self, q, para_list):
        mrc_input = MrcInput()
        mrc_input.question = q
        for p in para_list:
            p_input = MrcPassage()
            p_input.original = p["original"]
            p_input.morp = str(p["morp"])
            p_input.words.extend(p["words"])
            mrc_input.passages.extend([p_input])
        mrc_outputs = self.mrc_client.SendQuestion(mrc_input)
        return mrc_outputs.answers

    def run(self, question, passage_list):
        self.logger.info("Input: {}".format(question))
        self.start_time = self.entire_start_time = time.time()

        # question analysis
        q_morp = self.nlp.get_tree_result(question, original_token=False)
        self.time_check(stage="nlqa")

        result_dict = dict()
        for p in passage_list:
            doc_morp_list = list()
            doc_word_list = list()
            morp_list, word_list = self.nlp.get_tree_result(p, original_token=True)
            doc_morp_list += morp_list
            doc_word_list += word_list
            if self.DOC_TYPE in result_dict:
                result_dict[self.DOC_TYPE].append({"original": " ".join(doc_word_list),
                                               "morp": doc_morp_list,
                                               "words": doc_word_list})
            else:
                result_dict[self.DOC_TYPE] = [{"original": " ".join(doc_word_list),
                                           "morp": doc_morp_list,
                                           "words": doc_word_list}]

        passage_cnt_list = [len(result_dict[y]) for y in result_dict]
        self.logger.debug("Retrieved document cnt: {}".format(passage_cnt_list))
        if len(passage_cnt_list) <= 0:
            self.logger.error("There's no passage. Return None")
            return "", False
        else:
            para_list = list()
            for k in result_dict.keys():
                for p in result_dict[k]:
                    para_list.append(p)
            self.time_check(stage="Text processing(context) & Prepare MRC")

            mrc_outputs = self.mrc(str(q_morp), para_list)
            self.time_check(stage="MRC")

            final_list = list()
            for para, mrc_output in zip(para_list, mrc_outputs):
                final_list.append({"original": para['original'],
                                   "answer": mrc_output.answer, "prob": mrc_output.prob})
            return final_list

def f1_score(prediction, ground_truth):
    prediction_tokens = prediction.split()
    ground_truth_tokens = ground_truth.split()
    logging.debug(prediction_tokens)
    logging.debug(ground_truth_tokens)
    common = Counter(prediction_tokens) & Counter(ground_truth_tokens)
    num_same = sum(common.values())
    if num_same == 0:
        return 0
    precision = 1.0 * num_same / len(prediction_tokens)
    recall = 1.0 * num_same / len(ground_truth_tokens)
    f1 = (2 * precision * recall) / (precision + recall)
    return f1


if __name__ == '__main__':
    type = "f1"  # result, f1
    input_file = "../testcase_result/testcase_v3_release_v7.6_result_with_ctx.txt"
    #input_file = "../testcase_sample/testcase_v3_release_v7.2_result.txt"
    result_file = "../testcase_result/testcase_v3_release_v7.6_result_with_ctx_f1.txt"
    f_r = open(result_file, "a")

    header = True
    with open(input_file) as f:
        for line in f:
            if header:
                header = False
                continue
            if type == "result":
                # cate1	cate2	cate3	q_id	context	question	answer
                item = line.strip().split("\t")
                print("\nQuestion: %s" % item[5])

                mc = MrcClient(mrc_address='127.0.0.1', mrc_port=50001)
                result_list = mc.run(item[4], [item[6].strip('"')])
                for result in result_list:
                    f_r.write("\t".join(item+[result['answer'], str(result['prob'])]))
                    f_r.write("\n")
            elif type == "f1":
                # cate1	cate2	cate3	q_id	context	question	answer  model_answer    confidence
                item = line.strip().split("\t")
                exact_match = True if item[5] == item[7] else False
                f_r.write("\t".join(item+[str(exact_match), str(f1_score(item[7], item[5]))]))
                f_r.write("\n")
