#! /usr/bin/python
# -*- coding: UTF-8 -*-

import os, sys
import time
import logging
import pprint

import grpc
sys.path.append(os.path.join(os.environ['MRC_HOME'], 'pysrc'))
from morp_analyze_my import NLPAnalyzer
from minds_mrc_pb2 import MrcInput, MrcPassage, MRCStub


def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

class MrcClient:
    search_channel = None
    search_client = None

    def __init__(self, mrc_address, mrc_port):
        self.DOC_TYPE = "news"
        self.ANSWER_THRESHOLD = 0.7
        self.mrc_channel = grpc.insecure_channel('{}:{}'.format(mrc_address, mrc_port))
        self.mrc_client = MRCStub(self.mrc_channel)
        # for time check
        self.entire_start_time = None
        self.start_time = None
        # set logger
        self.logger = self.set_logger()
        self.nlp = NLPAnalyzer()

    def set_logger(self):
        log = logging.getLogger(__name__)
        log.setLevel(logging.DEBUG)
        # create log file: title is start time
        os.environ['TZ'] = 'Asia/Seoul'
        time.tzset()
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.INFO)
        formatter = logging.Formatter(fmt='[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s',
                                      datefmt='%Y-%m-%d_%T %Z')
        ch.setFormatter(formatter)
        log.addHandler(ch)
        return log

    def time_check(self, stage):
        self.logger.info("{}: {:.5}s".format(stage, float(time.time()-self.start_time)))
        self.start_time = time.time()

    def mrc(self, q, para_list):
        mrc_input = MrcInput()
        mrc_input.question = q
        for p in para_list:
            p_input = MrcPassage()
            p_input.original = p["original"]
            p_input.morp = str(p["morp"])
            p_input.words.extend(p["words"])
            mrc_input.passages.extend([p_input])
        mrc_outputs = self.mrc_client.SendQuestion(mrc_input)
        return mrc_outputs.answers

    def run(self, question, passage_list):
        self.logger.info("Input: {}".format(question))
        self.start_time = self.entire_start_time = time.time()

        # question analysis
        q_morp = self.nlp.get_tree_result(question, original_token=False)
        self.time_check(stage="nlqa")

        result_dict = dict()
        for p in passage_list:
            doc_morp_list = list()
            doc_word_list = list()
            morp_list, word_list = self.nlp.get_tree_result(p, original_token=True)
            doc_morp_list += morp_list
            doc_word_list += word_list
            if self.DOC_TYPE in result_dict:
                result_dict[self.DOC_TYPE].append({"original": " ".join(doc_word_list),
                                               "morp": doc_morp_list,
                                               "words": doc_word_list})
            else:
                result_dict[self.DOC_TYPE] = [{"original": " ".join(doc_word_list),
                                           "morp": doc_morp_list,
                                           "words": doc_word_list}]

        passage_cnt_list = [len(result_dict[y]) for y in result_dict]
        self.logger.debug("Retrieved document cnt: {}".format(passage_cnt_list))
        if len(passage_cnt_list) <= 0:
            self.logger.error("There's no passage. Return None")
            return "", False
        else:
            para_list = list()
            for k in result_dict.keys():
                for p in result_dict[k]:
                    para_list.append(p)
            self.time_check(stage="Text processing(context) & Prepare MRC")

            mrc_outputs = self.mrc(str(q_morp), para_list)
            self.time_check(stage="MRC")

            final_list = list()
            for para, mrc_output in zip(para_list, mrc_outputs):
                final_list.append({"original": para['original'],
                                   "answer": mrc_output.answer, "prob": mrc_output.prob})
            return final_list

if __name__ == '__main__':
    q = sys.argv[1].strip('"').rstrip("?")
    para_list = list()
    para_list.append('''주인공인 다니엘 블레이크를 연기한 데이브 존스는 코미디언이기도 하다. 그가 '나, 다니엘 블레이크를 인간으로서, 한 시민으로서 대우해 달라'고 천명하는 마지막 장면은 가슴 뭉클한 감동을 준다. 2등 상인 심사위원 대상을 받은 '단지 세상의 끝'도 12월 말로 개봉 시기를 조율 중이다. 엣나인필름 관계자는 "극장가 성수기 때 힘있게 가려고 연말 개봉을 목표로 하고 있다"며 "프랑스와 캐나다에서 먼저 개봉해야 하는 부분도 있다"고 말했다. '단지 세상의 끝'은 캐나다 출신 '칸의 총아' 자비에 돌란 감독이 2년 만에 내놓은 신작이다.''')
    para_list.append('''그는 노하우라고 한다면 잘 맞추려고 하는 편이라며 연기할 때 액션을 취하려고 하기보다 리액션을 많이 하려고 노력한다라고 말했다. 이어 상대방 이야기를 많이 들으려고 하고, 듣기 위해 끌어내려는 편이라며 난 누구든 맞출 수 있다는 생각을 가지고 있어야겠더라. 연기는 모놀로그가 아니니까라고 설명했다. 또 상대배역들에 대해 한 작품에서 만난다는게 굉장히 어려운 일이다. 시간, 생각도 맞아야 하고 그런 과정을 함께 할 때 감사함이 있다라며 닫혀있지 않으려고 노력한다고 덧붙였다.청년경찰은 의욕 충만 경찰대생 기준(박서준)과 이론 백단 경찰대생 희열(강하늘)이 외출을 나왔다가 납치 사건을 목격하면서 혈기왕성한 실전수사에 돌입하는 이야기를 그린 영화다. 오는 8월 9일 개봉. 김예랑''')
    para_list.append('''(서울=연합뉴스) 윤고은 기자 = MBC와 KBS 노조 총파업 4주차를 맞이하면서 방송 차질이 더욱 확대되고 있지만 드라마는 아직까지 한편을 제외하고는 문제가 없는 상황이다. 그 이유는 무엇일까. 지난 4일 양 방송사의 노조가 동시 파업에 돌입한 직후 뉴스, 라디오, 시사교양 프로그램은 즉각 차질을 빚기 시작했고 이어 예능 프로그램의 파행이 확대되고 있다. MBC는 첫주에 바로 드라마를 제외한 거의 모든 분야에서 차질이 빚어지고 있고, KBS는 파업 3주차부터 '해피선데이'와 '해피투게더' 등 예능 프로그램의 제작과 정상 방송이 중단됐다. ''')
    para_list.append('''그러나 양사 모두 드라마는 지난 24일까지 방송상으로는 아무런 차질이 없었다. MBC TV 월화극 '20세기 소년소녀'가 파업 여파로 예정된 25일 첫 방송을 못 맞춘 것이 드라마 첫 파행이었다. 이는 대부분의 드라마가 외주제작사에서 제작을 하는 데다, 예능 프로그램과 달리 배우, 스태프와의 계약이 복잡하게 얽힌 구도이기 때문이다. 16부작 미니시리즈 드라마의 경우, 사전제작 외에는 대개 4~5개월 내에 촬영이 완료돼야 한다. 외주제작사는 배우, 스태프와 한정된 기간 안에 촬영을 끝내는 것으로 계약하는데 파업 등의 문제로 제작이 지연되면 외주제작사가 손해를 고스란히 안게 되는 시스템이다. 이로 인해 외주제작사는 어떻게 해서든 예정된 스케줄 안에 제작해야 하고, 방송사는 이를 빌미로 외주제작사를 압박하면서 다른 분야와 달리 드라마에서는 웬만해서는 파업으로 인한 차질이 발생하지 않게 된다.''')
    para_list.append('''MBC TV '밥상 차리는 남자'도 메인 연출자가 파업에 동참하면서 촬영이 중단됐다가 지난 14일 재개됐는데 역시 같은 이유다. '밥상 차리는 남자'는 파업 직전인 지난 2일 시작했기 때문에 파업으로 결방되면 방송을 시작하자마자 중단하는 꼴이 된다. 드라마로서는 첫 방송이 연기되는 것보다 방송 도중 결방되는 게 더 큰 타격이다. 흐름이 끊겨버려 안 하느니만 못한 상황이 되기 때문이다. 그로 인해 메인 연출자의 파업 참여 부담이 더 커진다. 반면 예능 프로그램의 경우는 애초 출연자와의 출연 계약 기간이라는 것이 없어 파업으로 결방돼도 계약상 문제가 발생하는 경우가 거의 없고, 내용도 드라마처럼 연속성이 있는 게 아니라 결방의 부담이 드라마에 비해서는 현저히 적다. MBC노조 관계자는 "아직 확정적으로 말하긴 힘들지만 앞으로 시작하는 드라마의 경우는 대부분 제때 방송을 시작하기 쉽지 않을 것"이라며 "프로그램마다 사정이 다 복잡한 것은 사실이지만 파업이 길어지면 계획된 일정대로 가기 어렵다"고 전했다.''')
    print("\nQuestion: %s" % q)

    mc = MrcClient(mrc_address='127.0.0.1', mrc_port=50001)
    result_list = mc.run(q, para_list)
    for result in result_list:
        pprint.pprint(result)
        print("\n")
