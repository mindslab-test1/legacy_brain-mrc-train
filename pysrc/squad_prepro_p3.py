#! /usr/bin/env python
# -*- coding: UTF-8 -*-
# vi:ts=4:tw=200:shiftwidth=4
# vim600:fdm=marker
"""
author : Cheoneum Park <parkce3@gmail.ac.kr>
squad_prepro_p3.py: main program, preprocessing for SQuAD QA -> Korean dataset
"""

__author__ = 'Cheoneum Park'
__version__ = '2017-07-01'

import sys
import os
import ast
from prepro_utils_p3 import PreproUtils

s_util = PreproUtils()


def m_lst2lst_v5(m_lst):
    lst = []
    for l in m_lst:
        for sent in l:
            for w in sent:
                # print w,
                # if isinstance(w, str):
                # w = w.decode('utf8')	# python2
                # except: print w, type(w), sent; exit()
                lst.append(w)
    return lst


def prep4match_squad():
    data_path = os.path.join(os.environ['MRC_HOME'], 'data')
    rsc_path = os.path.join(os.environ['MRC_HOME'], 'resources')

    voca_file = open(rsc_path + '/kr/kor_vectors_50.sp.vocab', 'r', encoding='cp949')
    vocab_dic, voca_list = s_util.load_vocab4KR(voca_file)

    tr_data_dic = {}
    dev_data_dic = {}

    for name in ['dev', 'train']:
        print("Time to begin ... ", name)
        f_name = data_path + '/squad_ko_test/ko_test_' + name + '.json'
        kdocs = s_util.kdoc_open(f_name)
        kdocs_data = kdocs['data']

        x_lst = []
        q_lst = []
        a_lst = []
        x_str = []
        q_str = []
        qids = []
        total = 0

        for doc in kdocs_data:
            for p in doc['paragraphs']:
                mo_cont = m_lst2lst_v5(ast.literal_eval(p['context']))
                mo_cont.append('</s>')

                for qa in p['qas']:
                    ph_dic = {'ph_nnp': {}, 'ph': {}}
                    qids.append(qa['id'])
                    que = ast.literal_eval(qa['question'])
                    mo_q = m_lst2lst_v5(que)

                    q_lst.append(s_util.make_idx_list(mo_q, vocab_dic, ph_dic))
                    x_lst.append(s_util.make_idx_list(mo_cont, vocab_dic, ph_dic))
                    q_str.append(mo_q)
                    x_str.append(mo_cont)

                    for a in qa['answers']:
                        a_start = int(a['answer_start'])
                        a_end = int(a['answer_end']) - 1

                    a_lst.append([len(mo_cont) - 1, a_start, a_end])
            total += 1

            if (total + 1) % 1 == 0: sys.stderr.write('.')
            if (total + 1) % 10 == 0: sys.stderr.write(' ')
            if (total + 1) % 50 == 0: sys.stderr.write('\t')
            if (total + 1) % 100 == 0: sys.stderr.write(str(total + 1) + '\n')

        if 'train' == name:
            tr_data_dic = {'x_lst': x_lst,
                           'q_lst': q_lst,
                           'a_lst': a_lst,
                           'x_str': x_str,
                           'q_str': q_str,
                           'qids': qids,
                           'voca_list': voca_list}
        else:
            dev_data_dic = {'x_lst': x_lst,
                            'q_lst': q_lst,
                            'a_lst': a_lst,
                            'x_str': x_str,
                            'q_str': q_str,
                            'qids': qids,
                            'voca_list': voca_list}

    train_xc, train_qc, dev_xc, dev_qc, char2id = s_util.make_char_vocab(tr_data_dic['x_str'], tr_data_dic['q_str'],
                                                                         dev_data_dic['x_str'], dev_data_dic['q_str'])
    tr_data_dic['x_c_lst'] = train_xc
    tr_data_dic['q_c_lst'] = train_qc
    tr_data_dic['char2id'] = char2id
    dev_data_dic['x_c_lst'] = dev_xc
    dev_data_dic['q_c_lst'] = dev_qc
    dev_data_dic['char2id'] = char2id

    dic2msgpack(tr_data_dic, 'tr', data_path)
    dic2msgpack(dev_data_dic, 'dev', data_path)

def dic2msgpack(dic, file_name, fw_path):
    import msgpack as mp
    with open(fw_path + "/squad_ko_test/" + file_name + ".msgpack", 'wb') as f:
        mp.dump(dic, f, encoding='utf8')

if __name__ == "__main__":
    task = sys.argv[1]
    if task == "ko":
        prep4match_squad()