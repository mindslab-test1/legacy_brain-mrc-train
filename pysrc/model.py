# vi:ts=4:shiftwidth=4:expandtab
import torch
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
import logging

from torch.autograd import Variable
from utils import AverageMeter
from rnn_reader import RnnDocReader
from my_rnn_self_match_reader import RnnSelfMatchReader
from my_bidaf_reader import BidafReader
from my_r_net_reader import R_NetReader

# Modification:
#   - change the logger name
#   - save & load optimizer state dict
#   - change the dimension of inputs (for POS and NER features)
# Origin: https://github.com/facebookresearch/ParlAI/tree/master/parlai/agents/drqa

logger = logging.getLogger(__name__)


class DocReaderModel(object):
    """High level model that handles intializing the underlying network
    architecture, saving, updating examples, and predicting examples.
    """

    def __init__(self, opt, embedding=None, state_dict=None):
        # Book-keeping.
        self.opt = opt
        self.updates = state_dict['updates'] if state_dict else 0
        self.train_loss = AverageMeter()

        # Building network.
        if opt['model_type'] == 'drqa':
            self.network = RnnDocReader(opt, padding_idx=opt['padding_idx'], embedding=embedding)
        elif opt['model_type'] == 'drqa+sm':
            self.network = RnnSelfMatchReader(opt, padding_idx=opt['padding_idx'], embedding=embedding)
        elif opt['model_type'] == 'bidaf':
            self.network = BidafReader(opt, padding_idx=opt['padding_idx'], embedding=embedding)
        elif opt['model_type'] == 'r-net':
            self.network = R_NetReader(opt, padding_idx=opt['padding_idx'], embedding=embedding)
        else:
            raise RuntimeError('Unsupported model_type: %s' % opt['model_type'])
        if state_dict:
            new_state = set(self.network.state_dict().keys())
            for k in list(state_dict['network'].keys()):
                if k not in new_state:
                    del state_dict['network'][k]
            self.network.load_state_dict(state_dict['network'])

        # Building optimizer.
        parameters = [p for p in self.network.parameters() if p.requires_grad]
        if opt['optimizer'] == 'sgd':
            self.optimizer = optim.SGD(parameters, opt['learning_rate'],
                                       momentum=opt['momentum'],
                                       weight_decay=opt['weight_decay'])
        elif opt['optimizer'] == 'adamax':
            self.optimizer = optim.Adamax(parameters,
                                          weight_decay=opt['weight_decay'])
        else:
            raise RuntimeError('Unsupported optimizer: %s' % opt['optimizer'])
        if state_dict:
            self.optimizer.load_state_dict(state_dict['optimizer'])

    def update(self, ex):
        # Train mode
        self.network.train()

        # ex: (context_id, context_feature, context_mask, question_id, question_feature, question_mask, y_s, y_e, text, qid)
        # use_char: (context_id, context_feature, context_mask, question_id, question_feature, question_mask, context_cid, context_cmask, question_cid, question_cmask, y_s, y_e, text, qid)

        # Transfer to GPU
        if self.opt['cuda']:
            if self.opt['use_char']:
                inputs = [Variable(e.cuda(async=True)) for e in ex[:10]]
            else:
                inputs = [Variable(e.cuda(async=True)) for e in ex[:6]]
            target_s = Variable(ex[-4].cuda(async=True))
            target_e = Variable(ex[-3].cuda(async=True))
        else:
            if self.opt['use_char']:
                inputs = [Variable(e) for e in ex[:10]]
            else:
                inputs = [Variable(e) for e in ex[:6]]
            target_s = Variable(ex[-4])
            target_e = Variable(ex[-3])

        # Run forward
        score_s, score_e = self.network(*inputs)

        # Compute loss and accuracies
        loss = F.nll_loss(score_s, target_s) + F.nll_loss(score_e, target_e)
        self.train_loss.update(loss.data[0], ex[0].size(0))
        # test - by leeck
        #print(score_s)
        #print(score_e)
        #print(loss)
        #import pdb; pdb.set_trace()

        # Clear gradients and run backward
        self.optimizer.zero_grad()
        loss.backward()

        # Clip gradients
        torch.nn.utils.clip_grad_norm(self.network.parameters(),
                                      self.opt['grad_clipping'])

        # Update parameters
        self.optimizer.step()
        self.updates += 1

        # Reset any partially fixed parameters (e.g. rare words)
        self.reset_parameters()

    def predict(self, ex):
        # Eval mode
        self.network.eval()

        # ex: (context_id, context_feature, context_mask, question_id, question_feature, question_mask, y_s, y_e, text, qid)
        # use_char: (context_id, context_feature, context_mask, question_id, question_feature, question_mask, context_cid, context_cmask, question_cid, question_cmask, y_s, y_e, text, qid)
        # Transfer to GPU
        if self.opt['cuda']:
            if self.opt['use_char']:
                inputs = [Variable(e.cuda(async=True), volatile=True) for e in ex[:10]]
            else:
                inputs = [Variable(e.cuda(async=True), volatile=True) for e in ex[:6]]
        else:
            if self.opt['use_char']:
                inputs = [Variable(e, volatile=True) for e in ex[:10]]
            else:
                inputs = [Variable(e, volatile=True) for e in ex[:6]]
        # Run forward
        score_s, score_e = self.network(*inputs)

        # Transfer to CPU/normal tensors for numpy ops
        score_s = score_s.data.cpu()
        score_e = score_e.data.cpu()

        # Get argmax text spans
        text = ex[-2]
        predictions = []
        score_lst = []
        s_idx = -1  # fake data
        e_idx = -1  # fake data
        max_len = self.opt['max_len'] or score_s.size(1)
        for i in range(score_s.size(0)):
            scores = torch.ger(score_s[i], score_e[i])
            scores.triu_().tril_(max_len - 1)
            scores = scores.numpy()
            s_idx, e_idx = np.unravel_index(np.argmax(scores), scores.shape)
            out = ''
            for j in range(s_idx, e_idx+1):
                if out == '': out += text[i][j]
                else: out += ' ' + text[i][j]
            predictions.append(out)
            score_lst.append(scores[s_idx][e_idx])
            #import pdb; pdb.set_trace()
        return predictions, score_lst, (s_idx, e_idx)

    def get_answer(self, ex):
        # ex: (context_id, context_feature, context_mask, question_id, question_feature, question_mask, y_s, y_e, text, qid)
        # use_char: (context_id, context_feature, context_mask, question_id, question_feature, question_mask, context_cid, context_cmask, question_cid, question_cmask, y_s, y_e, text, qid)
        ans_s_idx = ex[-4].numpy()
        ans_e_idx = ex[-3].numpy()
        text = ex[-2]
        qid = ex[-1]
        answers = []
        #print(ans_s_idx)
        #for i in range(ans_s_idx.size(0)):
        for i in range(len(ans_s_idx)):
            ans = ''
            for j in range(int(ans_s_idx[i]), int(ans_e_idx[i]+1)):
                if ans == '': ans += text[i][j]
                else: ans += ' ' + text[i][j]
            #print('ans['+ans+']')
            answers.append([ans])

        return answers

    def reset_parameters(self):
        # Reset fixed embeddings to original value
        if self.opt['tune_partial'] > 0:
            offset = self.opt['tune_partial'] + 2
            if offset < self.network.embedding.weight.data.size(0):
                self.network.embedding.weight.data[offset:] \
                    = self.network.fixed_embedding

    def save(self, filename, epoch):
        params = {
            'state_dict': {
                'network': self.network.state_dict(),
                'optimizer': self.optimizer.state_dict(),
                'updates': self.updates
            },
            'config': self.opt,
            'epoch': epoch
        }
        try:
            torch.save(params, filename)
            logger.info('model saved to {}'.format(filename))
        except BaseException:
            logger.warn('[ WARN: Saving failed... continuing anyway. ]')

    def cuda(self):
        self.network.cuda()
