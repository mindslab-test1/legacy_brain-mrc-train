# coding=<utf-8>
# vi:ts=4:shiftwidth=4:expandtab
import re
import os
import sys
import time
import random
import string
import logging
import argparse
import pickle, gzip
import unicodedata
import collections
import numpy
from shutil import copyfile
from datetime import datetime
from collections import Counter
import torch
import msgpack
import pandas as pd
from model import DocReaderModel

parser = argparse.ArgumentParser(
    description='Train a Document Reader model.'
)

rsc_path = os.path.join(os.environ['MRC_HOME'], 'resources')
data_path = os.path.join(os.environ['MRC_HOME'], 'data')

### system
parser.add_argument('--log_per_updates', type=int, default=1000,
                    help='log model loss per x updates (mini-batches).')
parser.add_argument('--lang', default='kr', help='supported types: en, kr')
parser.add_argument('--train_data', default= data_path + 'squad_ko_test/tr.msgpack', help='path to train data file.')
parser.add_argument('--dev_data', default= data_path + 'squad_ko_test/dev.msgpack', help='path to test data file.')
parser.add_argument('--emb_file', default= rsc_path + 'kr/kor_vector.nnlm.h100.utf8.txt', help='path to word embedding file.')
parser.add_argument('--embedding_dim', type=int, default=100)
parser.add_argument('--model_dir', default='models/ko_mrc_v8.1', help='path to store saved models.')
#parser.add_argument('--save_last_only', action='store_true', help='only save the final models.')
parser.add_argument('--dont_save_last_only', action='store_false', dest='save_last_only', help='save models always.')
parser.add_argument('--save_min_f1', type=float, default=75.0, help='save when F1 is greater than min_f1.')
parser.add_argument('--eval_per_epoch', type=int, default=1, help='perform evaluation per x epoches.')
parser.add_argument('--seed', type=int, default=411, help='random seed for data shuffling, dropout, etc.')
parser.add_argument('--cuda', type=bool, default=torch.cuda.is_available(), help='whether to use GPU acceleration.')
### test
parser.add_argument('-t', '--test', default='',
                    help='previous model file name (in `model_dir`). '
                         'e.g. "best_model.pt"')
### training
parser.add_argument('-e', '--epoches', type=int, default=70)
parser.add_argument('-bs', '--batch_size', type=int, default=16)
parser.add_argument('-rs', '--resume', default='',
                    help='previous model file name (in `model_dir`). '
                         'e.g. "checkpoint_epoch_11.pt"')
parser.add_argument('-ro', '--resume_options', action='store_true',
                    help='use previous model options, ignore the cli and defaults.')
parser.add_argument('-rlr', '--reduce_lr', type=float, default=0.,
                    help='reduce initial (resumed) learning rate by this factor.')
parser.add_argument('-op', '--optimizer', default='adamax', help='supported optimizer: adamax, sgd')
parser.add_argument('-gc', '--grad_clipping', type=float, default=10)
parser.add_argument('-wd', '--weight_decay', type=float, default=0)
parser.add_argument('-lr', '--learning_rate', type=float, default=0.1, help='only applied to SGD.')
parser.add_argument('-mm', '--momentum', type=float, default=0, help='only applied to SGD.')
parser.add_argument('-tp', '--tune_partial', type=int, default=1000, help='finetune top-x embeddings.')
parser.add_argument('--fix_embeddings', action='store_true', help='if true, `tune_partial` will be ignored.')
parser.add_argument('--rnn_padding', action='store_true', help='perform rnn padding (much slower but more accurate).')
### model
parser.add_argument('--model_type', default='drqa+sm', help='supported types: drqa, drqa+sm, bidaf, r-net')
parser.add_argument('--question_merge', default='self_attn', help='self_attn or avg')
parser.add_argument('--doc_layers', type=int, default=5)
parser.add_argument('--question_layers', type=int, default=5)
parser.add_argument('--modeling_layers', type=int, default=2,
                    help='after BIDAF, Match LSTM, or RNN (for BIDAF, R-Net, DrQA+SM)')
parser.add_argument('--modeling2_layers', type=int, default=2,
                    help='after Self-Matching Attention (for R-Net)')
parser.add_argument('--hidden_size', type=int, default=100)
parser.add_argument('--use_char', dest='use_char', action='store_true')
parser.add_argument('--filter_sizes', default='3,4,5', help='for char CNN')
parser.add_argument('--num_filters', type=int, default=30, help='for char CNN')
parser.add_argument('--char_embed_dim', type=int, default=50)
parser.add_argument('--char_embed_dim2', type=int, default=50)
parser.add_argument('--exact_match', dest='exact_match', action='store_true')
parser.add_argument('--exact_match2', dest='exact_match2', action='store_true')
parser.add_argument('--num_features', type=int, default=2)
#parser.add_argument('--no_exact_match', dest='exact_match', action='store_false')
parser.add_argument('--use_qemb', action='store_true')
#parser.add_argument('--dont_use_qemb', dest='use_qemb', action='store_false')
parser.add_argument('--use_qemb2', action='store_true')
parser.add_argument('--concat_rnn_layers', action='store_true', help='for DrQA (BIDAF and R-Net ignore this option)')
#parser.add_argument('--dont_concat_rnn_layers', dest='concat_rnn_layers', action='store_false', help='for DrQA (BIDAF and R-Net ignore this option)')
parser.add_argument('--sum_rnn_layers', dest='sum_rnn_layers', action='store_true')
parser.add_argument('--dropout_emb', type=float, default=0.2)
parser.add_argument('--dropout_rnn', type=float, default=0.2)
#parser.add_argument('--dropout_rnn_output', action='store_true')
parser.add_argument('--no_dropout_rnn_output', dest='dropout_rnn_output', action='store_false')
#parser.add_argument('--max_len', type=int, default=15)
parser.add_argument('--max_len', type=int, default=50)
parser.add_argument('--rnn_type', default='sru', help='supported types: rnn, gru, lstm, sru')

args = parser.parse_args()

# set model dir
model_dir = args.model_dir
os.makedirs(model_dir, exist_ok=True)
model_dir = os.path.abspath(model_dir)

# set random seed
random.seed(args.seed)
torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

# setup logger
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
fh = logging.FileHandler(args.model_dir + '/output.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
log.addHandler(fh)
log.addHandler(ch)

# evaluation
def evaluate(batches, model, dev_ans_dict, opt):
    predictions = []
    qid_list = []
    score_lst = []
    dev_y_list = []
    dev_single_y_list = []
    for batch in batches:
        pred, pred_score, _ = model.predict(batch)
        ans = model.get_answer(batch)
        # normalize prediction and answer
        new_pred = []
        new_ans = []
        for p in pred:
            p = p.replace(' \'s', '\'s')
            p = p.replace(' ,', ',')
            p = p.replace(' - ', '-')
            p = p.replace(' / ', '/')
            p = p.replace(' ( ', ' (')
            p = p.replace(' ) ', ') ')
            p = p.replace("do n't", "don't")
            p = p.replace('US$ ', 'US$')
            p = p.replace('5 k resolution', '5k resolution')
            new_pred.append(p)
        for a_list in ans:
            new_a_list = []
            for a in a_list:
                a = a.replace(' \'s', '\'s')
                a = a.replace(' ,', ',')
                a = a.replace(' - ', '-')
                a = a.replace(' / ', '/')
                a = a.replace(' ( ', ' (')
                a = a.replace(' ) ', ') ')
                a = a.replace("do n't", "don't")
                a = a.replace('US$ ', 'US$')
                a = a.replace('5 k resolution', '5k resolution')
                new_a_list.append(a)
            new_ans.append(new_a_list)
        pred = new_pred
        ans = new_ans
        # end
        predictions.extend(pred)
        dev_single_y_list.extend(ans)
        score_lst.extend(pred_score)	# pred_score
        #import pdb; pdb.set_trace()
        qids = batch[-1]
        qid_list.extend(qids)
        if opt['lang'] == 'en':
            ans_list = [dev_ans_dict[qid] for qid in qids]
            dev_y_list.extend(ans_list)
    return predictions, dev_single_y_list, dev_y_list, score_lst

# write result
def write_result(predictions, dev_single_y_list, dev_y_list, score_list, opt):
    f = open(os.path.join(model_dir, 'result.txt'), 'w')
    if opt['lang'] == 'en':
        print('#', len(predictions), len(dev_y_list), len(dev_single_y_list), file=f)
        for pred, ans, single_ans in zip(predictions, dev_y_list, dev_single_y_list):
            print(pred + '\t', _normalize_answer(pred), '\t', ans, '\t', single_ans, file=f)
    else:
        print('#', len(predictions), len(dev_single_y_list), len(score_list), file=f)
        try:
            for pred, ans, score in zip(predictions, dev_single_y_list, score_list):
                print(pred + '\t', _normalize_answer(pred), '\t', ans, '\t', score, file=f)
        except UnicodeEncodeError:
            print('# UnicodeEncodeError!', file=f)
            pass
    return

# main
def main():
    log.info('[program starts.]')
    train, dev, dev_ans_dict, embedding, opt = load_data(vars(args))
    print('train:', len(train))
    print('dev:', len(dev))
    log.info('[Data loaded.]')

    if args.resume:
        log.info('[loading previous model...]')
        checkpoint = torch.load(os.path.join(model_dir, args.resume))
        if args.resume_options:
            opt = checkpoint['config']
        state_dict = checkpoint['state_dict']
        model = DocReaderModel(opt, embedding, state_dict)
        epoch_0 = checkpoint['epoch'] + 1
        for i in range(checkpoint['epoch']):
            random.shuffle(list(range(len(train))))  # synchronize random seed
        if args.reduce_lr:
            lr_decay(model.optimizer, lr_decay=args.reduce_lr)
    elif args.test:
        log.info('[loading previous model...]')
        checkpoint = torch.load(os.path.join(model_dir, args.test))
        opt = checkpoint['config']
        opt['max_len'] = args.max_len
        print('max_len:', opt['max_len'])
        state_dict = checkpoint['state_dict']
        model = DocReaderModel(opt, embedding, state_dict)
        epoch_0 = checkpoint['epoch']
    else:
        model = DocReaderModel(opt, embedding)
        epoch_0 = 1

    if args.cuda:
        model.cuda()

    if args.resume or args.test:
        batches = BatchGen(dev, batch_size=args.batch_size, evaluation=True, gpu=args.cuda, use_char=opt['use_char'],
                           padding_idx=opt['padding_idx'], char_padding_idx=opt['char_padding_idx'])
        predictions, dev_single_y_list, dev_y_list, score_list = evaluate(batches, model, dev_ans_dict, opt)
        if opt['lang'] == 'kr':
            em, f1 = score(predictions, dev_single_y_list)
            log.info("dev EM: {0:.2f} F1: {1:.2f}".format(em, f1))
        else:
            em, f1 = score(predictions, dev_single_y_list)
            log.info("dev(single answer) EM: {0:.2f} F1: {1:.2f}".format(em, f1))
            em, f1 = score(predictions, dev_y_list)
            log.info("dev EM: {0:.2f} F1: {1:.2f}".format(em, f1))
        best_val_em = em
        best_val_score = f1
        write_result(predictions, dev_single_y_list, dev_y_list, score_list, opt)
        if args.test: return
    else:
        best_val_em = 0.0
        best_val_score = 0.0

    best_val_epoch = epoch_0 - 1
    start0 = datetime.now()
    for epoch in range(epoch_0, epoch_0 + args.epoches):
        log.warn('Epoch {}'.format(epoch))
        # train
        batches = BatchGen(train, batch_size=args.batch_size, gpu=args.cuda, use_char=opt['use_char'], padding_idx=opt['padding_idx'])
        start = datetime.now()
        start_time = time.time() # by leeck
        for i, batch in enumerate(batches):
            model.update(batch)
            print('\rupdates[{0:6}] train loss[{1:.5f}] remaining[{2}]'.format(
                    model.updates, model.train_loss.avg,
                    str((datetime.now()-start) / (i+1) * (len(batches)-i-1)).split('.')[0]),
                    end='', file=sys.stderr)
            if i % args.log_per_updates == 0:
                print('\r', end='', file=sys.stderr)
                log.info('updates[{0:6}] train loss[{1:.5f}] remaining[{2}]'.format(
                    model.updates, model.train_loss.avg,
                    str((datetime.now()-start) / (i+1) * (len(batches)-i-1)).split('.')[0]))
            # test - by leeck
            #if i > 1: break
        total_time = time.time() - start_time
        print('\r', end='', file=sys.stderr)
        log.warn("#doc/sec: {0:.1f} traing time: {1}".format(len(train)/total_time, str(datetime.now() - start0).split('.')[0]))
        # eval
        if epoch % args.eval_per_epoch == 0:
            batches = BatchGen(dev, batch_size=args.batch_size, evaluation=True, gpu=args.cuda, use_char=opt['use_char'], padding_idx=opt['padding_idx'])
            predictions, dev_single_y_list, dev_y_list, score_list = evaluate(batches, model, dev_ans_dict, opt)
            if opt['lang'] == 'kr':
                em, f1 = score(predictions, dev_single_y_list)
                log.warn("dev EM: {0:.2f} F1: {1:.2f}".format(em, f1))
            else:
                em, f1 = score(predictions, dev_single_y_list)
                log.warn("dev(single answer) EM: {0:.2f} F1: {1:.2f}".format(em, f1))
                em, f1 = score(predictions, dev_y_list)
                log.warn("dev EM: {0:.2f} F1: {1:.2f}".format(em, f1))
            if f1 > best_val_score:
                write_result(predictions, dev_single_y_list, dev_y_list, score_list, opt)
        # save
        #if not args.save_last_only or epoch == epoch_0 + args.epoches - 1:
        if not args.save_last_only or epoch == epoch_0 + args.epoches - 1 \
            or (f1 > best_val_score and f1 >= args.save_min_f1):
            model_file = os.path.join(model_dir, 'checkpoint_epoch_{}.pt'.format(epoch))
            model.save(model_file, epoch)
            if f1 > best_val_score:
                best_val_em = em
                best_val_score = f1
                best_val_epoch = epoch
                copyfile(
                    model_file,
                    os.path.join(model_dir, 'best_model.pt'))
                log.info('[new best model saved.]')
        elif f1 > best_val_score:
            best_val_em = em
            best_val_score = f1
            best_val_epoch = epoch
            log.info('[new best model.]')

    log.warn("best model: epoch: {0} dev EM: {1:.2f} F1: {2:.2f}".format(best_val_epoch, best_val_em, best_val_score))


def lr_decay(optimizer, lr_decay):
    for param_group in optimizer.param_groups:
        param_group['lr'] *= lr_decay
    log.info('[learning rate reduced by {}]'.format(lr_decay))
    return optimizer


def load_embedding(file_name, word2id_dic, vocab_size, wv_dim):
    """ Loads word vectors from word2vec embedding (key value1 value2 ...)
    """
    embedding = 0.01 * numpy.random.randn(vocab_size, wv_dim)
    #with open(file_name) as f:
    with open(file_name, encoding='utf8') as f:
        count = 0
        for line in f:
            elems = line.split()
            token = elems[0]
            if token in word2id_dic:
                embedding[word2id_dic[token]] = [float(v) for v in elems[-wv_dim:]]
                count += 1
            elif token.encode('utf8') in word2id_dic:
                embedding[word2id_dic[token.encode('utf8')]] = [float(v) for v in elems[-wv_dim:]]
                count += 1
            #else: print(token, end=' ')
        print('load word embedding:', file_name, wv_dim, count)
    return embedding

def load_data(opt):
    # train set
    if 'pkl' in opt['train_data']:
        with open(opt['train_data'], 'rb') as f:
            #pickle_list = pickle.load(f)
            pickle_list = pickle.load(f, encoding='utf8')
    elif 'msgpack' in opt['train_data']:
        with open(opt['train_data'], 'rb') as f:
            data = msgpack.load(f, encoding='utf8')
            #pickle_list = data['x_lst'], data['q_lst'], data['a_lst'], data['x_str'], data['q_str'], data['qids'], data['voca_list']
            pickle_list = data['x_lst'], data['q_lst'], data['x_c_lst'], data['q_c_lst'], data['a_lst'], data['x_str'], data['q_str'], data['qids'], data['voca_list'], data['char2id']
    else:
        print('Unknown file:', f)
        sys.exit()

    if len(pickle_list) == 6: # for squad5 data ?
        train_x, train_q, train_y, train_x_str, train_qids, vocab_x_list = pickle_list
        train_q_str = []
    elif len(pickle_list) == 7: # for Korean data
        train_x, train_q, train_y, train_x_str, train_q_str, train_qids, vocab_x_list = pickle_list
        #print('vocab_x:', vocab_x_list[:50])
    elif len(pickle_list) == 10: # for Korean data v7
        train_x, train_q, train_xc, train_qc, train_y, train_x_str, train_q_str, train_qids, vocab_x_list, opt['char2id'] = pickle_list
    elif len(pickle_list) == 11: # for squad6
        train_x, train_q, train_y, train_x_str, train_q_str, train_qids, _, _, _, _, vocab_x_list = pickle_list
    else:
        print('Unknown Pickle file:', len(pickle_list))
        sys.exit()
    # dev set
    if 'pkl' in opt['dev_data']:
        with open(opt['dev_data'], 'rb') as f:
            #pickle_list = pickle.load(f)
            pickle_list = pickle.load(f, encoding='utf8')
    elif 'msgpack' in opt['dev_data']:
        with open(opt['dev_data'], 'rb') as f:
            data = msgpack.load(f, encoding='utf8')
            #pickle_list = data['x_lst'], data['q_lst'], data['a_lst'], data['x_str'], data['q_str'], data['qids'], data['voca_list']
            pickle_list = data['x_lst'], data['q_lst'], data['x_c_lst'], data['q_c_lst'], \
                          data['a_lst'], data['x_str'], data['q_str'], data['qids'], data['voca_list'], data['char2id']
    if len(pickle_list) == 6:
        dev_x, dev_q, dev_y, dev_x_str, dev_qids, _ = pickle_list
        dev_q_str = []
    elif len(pickle_list) == 7:
        dev_x, dev_q, dev_y, dev_x_str, dev_q_str, dev_qids, _ = pickle_list
    elif len(pickle_list) == 10:
        dev_x, dev_q, dev_xc, dev_qc, dev_y, dev_x_str, dev_q_str, dev_qids, _, _ = pickle_list
    elif len(pickle_list) == 11:
        dev_x, dev_q, dev_y, dev_x_str, dev_q_str, dev_qids, _, _, _, _, _ = pickle_list
    else:
        print('Unknown Pickle file:', len(pickle_list))
        sys.exit()
    print('train data size:', len(train_x), len(train_q), len(train_y), len(train_x_str), len(train_q_str), len(train_qids))
    print('dev data size:', len(dev_x), len(dev_q), len(dev_y), len(dev_x_str), len(dev_q_str), len(dev_qids))
    if opt['use_char']:
        print ('train char:', len(train_xc), len(train_qc)); print ('dev char:', len(dev_xc), len(dev_qc))
        opt['char_vocab_size'] = len(opt['char2id'])
        print('\nchar_vocab_size:', opt['char_vocab_size'])
    # train_y -> train_ys and train ye
    train_ys = []
    train_ye = []
    for y in train_y:
        train_ys.append(int(y[1]))
        train_ye.append(int(y[2]))
    # dev_y -> dev_ys and dev ye
    dev_ys = []
    dev_ye = []
    for y in dev_y:
        dev_ys.append(int(y[1]))
        dev_ye.append(int(y[2]))
    #print('dev_x[0]:', dev_x[0])
    print('dev_q[0]:', dev_q[0])
    if opt['use_char']: print('dev_qc[0]:', dev_qc[0])
    print('dev_y[0]:', dev_y[0])
    print('dev_ys[0]:', dev_ys[0])
    print('dev_ye[0]:', dev_ye[0])
    #print('dev_x_str[0]:', dev_x_str[0])
    #print('dev_q_str[0]:', dev_q_str[0])
    print('dev_qids[0]:', dev_qids[0])
    # load word vocab.
    opt['word2id'] = {}
    for i, w in enumerate(vocab_x_list):
        if w in opt['word2id']:
            try: print('Warning(vocab_word):', w , 'is exist!', opt['word2id'][w], i)
            except: pass
        opt['word2id'][w] = i
    if 'eos' not in opt['word2id']:
        i += 1
        vocab_x_list.append('eos')
        opt['word2id']['eos'] = i
        print('"eos" is appended to vocab.')
    print('vocab_x size:', i+1)
    opt['pretrained_words'] = True
    opt['vocab_size'] = i+1
    # init char vocab.
    opt['char_padding_idx'] = 0
    # load word embedding
    emb_numpy = load_embedding(opt['emb_file'], opt['word2id'], opt['vocab_size'], opt['embedding_dim'])
    embedding = torch.Tensor(emb_numpy)
    # padding_idx
    # eos for batch padding
    if '<PAD>' in opt['word2id']: padding_idx = opt['word2id']['<PAD>']
    elif '<pad>' in opt['word2id']: padding_idx = opt['word2id']['<pad>']
    elif '</s>' in opt['word2id']: padding_idx = opt['word2id']['</s>']
    elif 'EOS' in opt['word2id']: padding_idx = opt['word2id']['EOS']
    elif 'eos' in opt['word2id']: padding_idx = opt['word2id']['eos']
    elif 'UNK' in opt['word2id']: padding_idx = opt['word2id']['UNK']
    elif 'unk' in opt['word2id']: padding_idx = opt['word2id']['unk']
    else: padding_idx = 0
    opt['padding_idx'] = padding_idx
    print('batch padding idx:', padding_idx, vocab_x_list[padding_idx])
    # exact_match , TF feature
    def make_feature(train_x, train_q, train_x_str, train_q_str):
        for x, q, xs, qs in zip(train_x, train_q, train_x_str, train_q_str): # using word idx
            if len(x) != len(xs) or len(q) != len(qs):
                print(len(x), len(xs), len(q), len(qs))
                import pdb; pdb.set_trace()
        train_f = []
        #for x, q in zip(train_x, train_q): # using word idx
        for x, q in zip(train_x_str, train_q_str): # using word string
            f = [wid in q for wid in x]
            counter_ = collections.Counter(wid for wid in x)
            total = sum(counter_.values()) + 1e-5
            tf = [counter_[wid] / total for wid in x]
            train_f.append(list(zip(f, tf)))
        return train_f
    train_xf = make_feature(train_x, train_q, train_x_str, train_q_str)
    dev_xf = make_feature(dev_x, dev_q, dev_x_str, dev_q_str)
    train_qf = make_feature(train_q, train_x, train_q_str, train_x_str)
    dev_qf = make_feature(dev_q, dev_x, dev_q_str, dev_x_str)
    #print('dev_f[0]:', dev_f[0])
    print('exact_match and TF features are generated.')
    # train, dev data
    if opt['use_char']:
        train = list(zip(train_x, train_xf, train_q, train_qf, train_xc, train_qc, train_ys, train_ye, train_x_str, train_qids))
        dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_xc, dev_qc, dev_ys, dev_ye, dev_x_str, dev_qids))
    else:
        train = list(zip(train_x, train_xf, train_q, train_qf, train_ys, train_ye, train_x_str, train_qids))
        dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_ys, dev_ye, dev_x_str, dev_qids))
    # for dev_y_list (for multiple answers)
    if opt['lang'] == 'en':
        dev_orig = pd.read_csv('data/squad6/dev.csv', encoding='utf8')
        dev_qid_list = dev_orig['id'].tolist()[:len(dev)]
        dev_ans_list = dev_orig['answers'].tolist()[:len(dev)]
        dev_ans_dict = {}
        for qid, ans in zip(dev_qid_list, dev_ans_list):
            dev_ans_dict[qid] = eval(ans)
    else:
        dev_ans_dict = {}
    return train, dev, dev_ans_dict, embedding, opt


class BatchGen:
    def __init__(self, data, batch_size, gpu, evaluation=False, use_char=False, padding_idx=0, char_padding_idx=0):
        '''
        input:
            data - list of lists
            batch_size - int
        '''
        self.batch_size = batch_size
        self.evaluation = evaluation
        self.gpu = gpu
        self.use_char = use_char
        self.padding_idx = padding_idx
        self.char_padding_idx = char_padding_idx

        # shuffle
        if not evaluation:
            indices = list(range(len(data)))
            random.shuffle(indices)
            data = [data[i] for i in indices]
        # chunk into batches
        data = [data[i:i + batch_size] for i in range(0, len(data), batch_size)]
        self.data = data

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        for batch in self.data:
            batch_size = len(batch)
            batch = list(zip(*batch))
            if self.use_char:
                assert len(batch) == 10
            else:
                assert len(batch) == 8

            # batch: list(zip(train_x, train_xf, train_q, train_qf, train_ys, train_ye, train_x_str, train_qids))
            # use_char: list(zip(train_x, train_xf, train_q, train_qf, train_xc, train_qc, train_ys, train_ye, train_x_str, train_qids))
            # train_x
            context_x_len = max(len(x) for x in batch[0])
            context_id = torch.LongTensor(batch_size, context_x_len).fill_(self.padding_idx)
            for i, doc in enumerate(batch[0]):
                context_id[i, :len(doc)] = torch.LongTensor(doc)

            # train_xf
            context_xf_len = max(len(x) for x in batch[1])
            feature_len = len(batch[1][0][0])
            context_feature = torch.Tensor(batch_size, context_xf_len, feature_len).fill_(0)
            # test - by leeck
            if context_x_len != context_xf_len:
                print('Error: context_x_len != context_xf_len')
                print('context_x_len=', context_x_len, 'context_xf_len=', context_xf_len)
                print(context_id.size(), context_feature.size())
                import pdb; pdb.set_trace()
            for i, doc in enumerate(batch[1]):
                for j, feature in enumerate(doc):
                    context_feature[i, j, :] = torch.Tensor(feature)

            # train_q
            question_len = max(len(x) for x in batch[2])
            question_id = torch.LongTensor(batch_size, question_len).fill_(self.padding_idx)
            for i, doc in enumerate(batch[2]):
                try: question_id[i, :len(doc)] = torch.LongTensor(doc)
                except: import pdb; pdb.set_trace()

            # train_qf
            context_qf_len = max(len(x) for x in batch[3])
            feature_len = len(batch[3][0][0])
            question_feature = torch.Tensor(batch_size, context_qf_len, feature_len).fill_(0)
            # test - by leeck
            if question_len != context_qf_len:
                print('Error: question_len != context_qf_len')
                print('question_len=', question_len, 'context_f_len=', context_qf_len)
                print(question_id.size(), question_feature.size())
                import pdb; pdb.set_trace()
            for i, q in enumerate(batch[3]):
                for j, feature in enumerate(q):
                    question_feature[i, j, :] = torch.Tensor(feature)

            # mask
            context_mask = torch.eq(context_id, self.padding_idx)
            question_mask = torch.eq(question_id, self.padding_idx)

            if self.use_char:
                # train_xc
                context_x_len = max(len(x) for x in batch[4])
                context_c_len = -1
                for x in batch[4]:
                    max_c_len = max(len(c) for c in x)
                    if max_c_len > context_c_len: context_c_len = max_c_len
                context_cid = torch.LongTensor(batch_size, context_x_len, context_c_len).fill_(self.char_padding_idx)
                for i, x in enumerate(batch[4]):
                    for j, w in enumerate(x):
                        context_cid[i, j, :len(w)] = torch.LongTensor(w)
                # train_qc
                question_len = max(len(q) for q in batch[5])
                question_c_len = -1
                for q in batch[5]:
                    max_c_len = max(len(c) for c in q)
                    if max_c_len > question_c_len: question_c_len = max_c_len
                question_cid = torch.LongTensor(batch_size, question_len, question_c_len).fill_(self.char_padding_idx)
                for i, q in enumerate(batch[5]):
                    for j, w in enumerate(q):
                        try: question_cid[i, j, :len(w)] = torch.LongTensor(w)
                        except: import pdb; pdb.set_trace()
                # mask
                context_cmask = torch.eq(context_cid, self.char_padding_idx)
                question_cmask = torch.eq(question_cid, self.char_padding_idx)

            # train_ys, train_ye
            y_s = torch.LongTensor(batch[-4])
            y_e = torch.LongTensor(batch[-3])

            # train_x_str
            text = list(batch[-2])

            # train_qids
            qid = list(batch[-1])

            if self.gpu:
                context_id = context_id.pin_memory()
                context_feature = context_feature.pin_memory()
                context_mask = context_mask.pin_memory()
                question_id = question_id.pin_memory()
                question_mask = question_mask.pin_memory()
                y_s = y_s.pin_memory()
                y_e = y_e.pin_memory()
                if self.use_char:
                    context_cid = context_cid.pin_memory()
                    question_cid = question_cid.pin_memory()
                    context_cmask = context_cmask.pin_memory()
                    question_cmask = question_cmask.pin_memory()
            if self.use_char:
                yield (context_id, context_feature, context_mask, question_id, question_feature, question_mask, context_cid, context_cmask, question_cid, question_cmask, y_s, y_e, text, qid)
            else:
                yield (context_id, context_feature, context_mask, question_id, question_feature, question_mask, y_s, y_e, text, qid)


def _normalize_answer(s):
    def remove_articles(text):
        return re.sub(r'\b(a|an|the)\b', ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def _exact_match(pred, answers):
    if pred is None or answers is None:
        return False
    pred = _normalize_answer(pred)
    for a in answers:
        if pred == _normalize_answer(a):
            return True
    return False


def _f1_score(pred, answers):
    def _score(g_tokens, a_tokens):
        common = Counter(g_tokens) & Counter(a_tokens)
        num_same = sum(common.values())
        if num_same == 0:
            return 0
        precision = 1. * num_same / len(g_tokens)
        recall = 1. * num_same / len(a_tokens)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1

    if pred is None or answers is None:
        return 0
    g_tokens = _normalize_answer(pred).split()
    scores = [_score(g_tokens, _normalize_answer(a).split()) for a in answers]
    return max(scores)


def score(pred, truth):
    assert len(pred) == len(truth)
    f1 = em = total = 0
    for p, t in zip(pred, truth):
        total += 1
        em += _exact_match(p, t)
        f1 += _f1_score(p, t)
    em = 100. * em / total
    f1 = 100. * f1 / total
    return em, f1

if __name__ == '__main__':
    main()
