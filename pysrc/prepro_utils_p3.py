#! /usr/bin/env python
# -*- coding: UTF-8 -*-
# vi:ts=4:tw=200:shiftwidth=4
# vim600:fdm=marker
"""
author : Cheoneum Park <parkce3@gmail.ac.kr>
squad_utils.py: utils in SQuAD QA
"""

__author__ = 'Cheoneum Park'
__version__ = '2017-07-01'

import json

class PreproUtils:
    def kdoc_open(self, f):
        doc_file = open(f, "r")
        kdoc = json.loads(doc_file.read())
        doc_file.close()
        return kdoc

    def load_vocab4KR(self, voca_file):
        vocab_dic = {'<PAD>': 0, '<UNK>': 1}
        voca_list = [0, 1]
        for i, line in enumerate(voca_file.read().split('\n')):
            try: line = line.split("\n")[0].lower()
            except: line = line.split("\n")[0].lower()
            vocab_dic[line] = i+2
            voca_list.append(line)
        return vocab_dic, voca_list

    def make_idx_list(self, lnt_list, vocab_dic, ph_dic):
        idx_list = []
        for lemma in lnt_list:
            try: idx_list.append(vocab_dic[lemma])
            except:
                pos = lemma.split('/')[-1]
                try: idx_list.append(vocab_dic[pos])
                except: idx_list.append(vocab_dic['<UNK>'])
        return idx_list

    def make_char_vocab(self, train_x_str, train_q_str, dev_x_str, dev_q_str, char2id=None, data_dir=None):
        print('make char data ...')
        if char2id is None:
            char2id = {'<PAD>':0, 'UNK':1}
            for d in train_x_str:
                for w in d:
                    for c in w:
                        if c not in char2id:
                            char2id[c] = len(char2id)
                            try:
                                if len(char2id) % 100 == 0:
                                    print(c, end=' ')
                            except: pass
        print('\nchar_vocab_size:', len(char2id))
        print('make train_xc, train_qc, dev_xc, dev_qc ...')
        def make_cx(x_str):
            xc = []
            for d in x_str:
                w_list = []
                for w in d:
                    c_list = []
                    for c in w:
                        if c in char2id: c_list.append(char2id[c])
                        else: c_list.append(char2id['UNK'])
                    w_list.append(c_list)
                xc.append(w_list)
            return xc
        train_xc = make_cx(train_x_str)
        train_qc = make_cx(train_q_str)
        dev_xc = make_cx(dev_x_str)
        dev_qc = make_cx(dev_q_str)
        return train_xc, train_qc, dev_xc, dev_qc, char2id
